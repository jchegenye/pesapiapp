<!DOCTYPE html>
<html lang="en">
  <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    	<title>Home | Online Estate Managment System</title>

    	<?php include("views/partials/css-links.php"); ?>

	</head>

	<body>
	    <nav class="navbar navbar-default navbar-fixed-top">
	        <div class="container">
	            <!-- Brand and toggle get grouped for better mobile display -->
	            <div class="navbar-header page-scroll">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="#page-top">O E M S</a>
	            </div>

	            <!-- Collect the nav links, forms, and other content for toggling -->
	            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	                <ul class="nav navbar-nav navbar-right">
	                    <li class="hidden">
	                        <a href="#page-top"></a>
	                    </li>
	                    <li class="page-scroll">
	                        <a href="index.php">Login</a>
	                    </li>
	                </ul>
	            </div>
	            <!-- /.navbar-collapse -->
	        </div>
	        <!-- /.container-fluid -->
	    </nav>

		<div class="container">
			
			<div class="col-md-8">
				<div class="row">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel" style="margin-top: 65px;">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active">
								<img style="width: 100%; height: 543px; border-radius:5px; " src="public/assets/random/houses/5.jpg" class="girl img-responsive" alt="" />
							</div>
							<div class="item">
								<img style="width: 100%; height: 543px; border-radius:5px;" src="public/assets/random/houses/4.jpg" class="girl img-responsive" alt="" />
							</div>
							<div class="item">
								<img style="width: 100%; height: 543px; border-radius:5px;" src="public/assets/random/houses/5.jpg" class="girl img-responsive" alt="" />
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div style="
	margin-top: 65px;
    margin-left: 15px;
    margin-right: 15px;">
						<h4 style="
	overflow: hidden;
    padding: 16px 20px 16px 20px;
    clear: both;
    background: url('public/assets/random/tail_footer.gif') 0 0px repeat-x;
    border-bottom: 1px solid #FFF;
    border-radius: 3px 3px 0 0;">Browse</h4>
    				<a href="apartments.php">
    					<img style="width: 100%; height:150px;" src="public/assets/random/apartments/1.jpg" class="girl img-responsive" alt="" />
							<div style="
							    padding-left: 15px;
							    padding-top: 10px;
							    padding-bottom: 10px;
							    background: #FFFAF2;
							">
								<h5>Apartments</h5>
								<p>View more & manage apartments information</p>
							</div>
						</a>
					</div>
					
					<div style="
						margin-top:10px;
					    margin-left: 15px;
					    margin-right: 15px;">
					    <a href="houses.php">
	    					<img style="width: 100%; height:150px;" src="public/assets/random/houses/5.jpg" class="girl img-responsive" alt="" />
							<div style="
							    padding-left: 15px;
							    padding-top: 10px;
							    padding-bottom: 10px;
							    background: #FFFAF2;
							">
								<h5>Houses</h5>
								<p>View more & manage information</p>
							</div>
						</a>
					</div>
				</div>
			</div>

		</div>

		<div class="container">
			<h5 class="text-center">Developed by jchegenye</h5>
		</div>
		<?php include("views/partials/js-links.php"); ?>
	</body>

</html>