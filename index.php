<!DOCTYPE html>
<html lang="en">
  <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    	<title>Login | Online Estate Managment System</title>

    	<?php include("views/partials/css-links.php"); ?>
	</head>

	<body class="login-img3-body">

		<div class="container">

			<!-- <form class="login-form" action="views/payment-method.php" method="POST"> -->
			<form class="login-form" action="views/processors/login_processor.php" method="POST">

		        <div class="login-wrap">
		            <p class="login-img"><i class="icon_lock_alt"></i></p>
		            <?php

						if(isset($_GET['msg'])){
						
							$data=$_GET['msg'];
							$msg =''.$data.'';

								?>
									<div class="alert alert-danger text-center">
										<?php echo $msg; ?>	
									</div>
								<?php
						}

					?>
		            <div class="input-group">
		              <span class="input-group-addon"><i class="icon_phone"></i></span>
		              <input type="text" name="phonenumber" class="form-control" placeholder="Phone Number" autofocus>
		            </div>
		            <div class="input-group">
		                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
		                <input type="password" name="password" class="form-control" placeholder="Password">
		            </div>
		            <label class="checkbox">
		                <input type="checkbox" value="remember-me"> Remember me
		                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
		            </label>
		            <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
		            <a class="btn btn-info btn-lg btn-block" href="views/signup.php">Signup</a>
		            <a class="btn btn-info btn-lg btn-block" href="home.php" style="background: #FF6347; border: #FF6347;">Go Back To Browser</a>
		        </div>
	      	</form>
			
		</div>

		<?php include("views/partials/js-links.php"); ?>
	</body>

</html>