<!DOCTYPE html>
<html lang="en">

  <!-- 
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech> 
  -->

  <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    	<title>Payment Method | Online Estate Managment System</title>

    	<?php include("partials/css-links.php"); ?>
	</head>

	<body class="login-img3-body">

		<div class="container">

			<form class="login-form payment" action="signup-processor.php" method="POST">        
		        <div class="login-wrap">
		            <p class="login-img"><i class="icon_cogs"></i></p>
		            <hr>
		            <p>Select your payment plan</p>
		            <div class="payment-tabs text-center">
			            <a href="../Api/pesaPi-master/php/webroot/index.php" class="payment-tab">M-Pesa</a>
			            <a href="" class="payment-tab">PayPal</a>
			        </div>
		        </div>
		      </form>
			
		</div>

		<?php include("partials/js-links.php"); ?>
	</body>

</html>