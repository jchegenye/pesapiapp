<!DOCTYPE html>
<html lang="en">

  <!-- 
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
  -->

  <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    	<title>Signup | Online Estate Managment System</title>

    	<?php include("partials/css-links.php"); ?>
	</head>

	<body class="login-img3-body">
	
		<script type="text/javascript">
			function checkForm(form) {

				if(form.names.value == "") { alert("Error: Name Cannot be blank!"); 
				form.names.focus(); return false; }

				if(form.phonenumber.value.length < 10) { alert("Error: Phone Number should be minimum of eight numerics, Only numerics are required!"); 
				form.phonenumber.focus(); return false; }
				re = /[0-10]/; if(!re.test(form.phonenumber.value)) 
				{ alert("Error: Phone Number must contain numerics only(0-10)!"); form.phonenumber.focus(); return false; }
				
				if(form.password.value != "" && form.password.value == form.confrm_password.value) 
				{ if(form.password.value.length < 6)
				{ alert("Error: Password must contain at least six characters!"); form.password.focus(); return false; }
				if(form.password.value == form.random_id.value) { alert("Error: Password must be different from Username!");
				form.password.focus(); return false; } re = /[0-9]/; if(!re.test(form.password.value)) 
				{ alert("Error: password must contain at least one number (0-9)!"); form.password.focus(); return false; }
				re = /[a-z]/; if(!re.test(form.password.value)) { alert("Error: password must contain at least one lowercase letter (a-z)!");
				form.password.focus(); return false; } re = /[A-Z]/; if(!re.test(form.password.value)) 
				{ alert("Error: password must contain at least one uppercase letter (A-Z)!"); 
				form.password.focus(); return false; } } else { alert("Error: Please check that you've entered and confirmed password!"); 
				form.password.focus(); return false; } alert("Your details are valid: " + form.random_id.value); 
				return true; 

			} 
		</script>

		<div class="container">

			<form onsubmit="return checkForm(this);" class="login-form" action="processors/signup_processor.php" method="POST">
			      
		        <div class="login-wrap">
		            <p class="login-img"><i class="icon_lock_alt"></i></p>
		            <?php

						if(isset($_GET['msg'])){
						
							$data=$_GET['msg'];
							$msg =''.$data.'';

								?>
									<div class="alert alert-danger text-center">
										<?php echo $msg; ?>	
									</div>
								<?php
						}

					?> 
		            <input type="hidden" name="usertype" value="guest">
		            <input type="hidden" name="count_approval" value="1">
		            <input type="hidden" name="registered_date" value="<?php echo date('Y-m-d'); ?>">
		            <div class="input-group">
		              <span class="input-group-addon"><i class="icon_profile"></i></span>
		              <input type="text" name="names" class="form-control" placeholder="Full Names" autofocus>
		            </div>
		            <div class="input-group">
		              <span class="input-group-addon"><i class="icon_phone"></i></span>
		              <input type="text" name="phonenumber" class="form-control" placeholder="Phone Number" autofocus>
		            </div>
		            <div class="input-group">
		                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
		                <input type="password" name="password" class="form-control" placeholder="Password">
		            </div>
		            <div class="input-group">
		                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
		                <input type="password" name="confrm_password" class="form-control" placeholder="Confirm Password">
		            </div>
		            <label class="checkbox">
		                <input type="checkbox" value="remember-me"> Remember me
		                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
		            </label>
		            <button class="btn btn-info btn-lg btn-block" type="submit">Signup</button>
		            <a class="btn btn-primary btn-lg btn-block" href="../index.php">Login</a>
		            <a class="btn btn-info btn-lg btn-block" href="../home.php" style="background: #FF6347; border: #FF6347;">Go Back To Browser</a>
		        </div>
		      </form>
			
		</div>

		<?php include("partials/js-links.php"); ?>
	</body>

</html>