<?php
/*
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/
	session_start();
	session_destroy();
	unset($_SESSION["user_id"]);
	unset($_SESSION["phonenumber"]);
	unset($_SESSION["usertype"]);
	header("location:../../index.php?msg=You%20have%20Successfully%20logged%20out!&type=information");
?>