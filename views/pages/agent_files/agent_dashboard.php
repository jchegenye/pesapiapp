<?php session_start();
/**
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/

    include("header.php");

?>

<div class="container">

	<div class="row">
		<div class="col-md-12">
			<h3 class="page-header">
				<i class="fa fa-list-alt"></i>
				Dashboard 
			</h3>
		</div>
	</div>

		
	<?php 
		$page = $_SESSION['page'];
		include("pages/$page");
	?>
	
	<div class="panel" style="margin-top: 15px;">
	  	<div class="panel-body">
			<i class="fa fa-copyright"> 2016 Copyright | Developed By <a href="#">Chegenye</a> 
	  	</div>
	</div>
</div>


