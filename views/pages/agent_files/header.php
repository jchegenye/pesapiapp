<?php
/**
    * @Author: Jackson Chegenye
    * @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
    * @Version: 0.0.5v
    * @Location: Nairobi - Kenya
    * @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/ 
    include_once("init_agent.php");
    include "../../dbconnector.php";

  ?>
  <title>Agent's Dashboard</title>
  
  <?php include("../../partials/css-links.php"); ?>
  <?php include("../../partials/js-links.php"); ?>

  <div class="container">
    <nav class="navbar navbar-default">
        <div class="navbar-header">
          	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            	<span class="sr-only">Toggle navigation</span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
          	</button>
          	<a class="navbar-brand" href="admins_dashboard.php">OEMS</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <!--  <ul class="nav navbar-nav">
                <li class="active">
                  <a href="agent_dashboard.php">Home</a>
                </li>
            </ul> -->
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <p class="alert alert-info welcome">
                        Welcome <?php echo $ADMINS['usertype'] ?>, 
                        <?php echo $ADMINS['phonenumber'] ?>!
                    </p>
                </li>
                <li>
                    <a href="../../logout.php" class="btn btn-danger" style="color: #FFF; background-color: #FF2D55; border: 0px solid;">Logout<span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
     
</div>

<!-- This is the script for printing -->
<script language="javascript" type="text/javascript">
  function printDiv(divName){
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }
</script>