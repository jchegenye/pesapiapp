<?php ?>

<!--
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
-->
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<?php include('partials/sub_links.php'); ?>					  	
			</ol>
		</div>
	</div>

<?php if ($_SESSION["editor"] == ""){ 
	
	//Change this to autoselect number of house units in an apartment
	$gen_code =$db->maxOfAll ("id_houses","houses");
	$gen_code= $gen_code+1;
	$code="A".$gen_code;

	  	if(isset($_GET['message'])){
	
		$data=$_GET['message'];
		$msg =''.$data.'';

		?>
			<div class="alert alert-success text-center"><?php echo  $msg; ?></div>
		<?php
	}

?>
	<section class="panel">
		<header class="panel-heading">
			Add House
		</header>
		<div class="panel-body">

			<form method="post" action="config/processors/addhouse.php" enctype="multipart/form-data">
				<input type="hidden" name="house_reg_date" value="<?php echo date('Y-m-d'); ?>">
				<table>
					<tbody>
						<tr>
							<td>
								<div class="form-group">
									<label for="house_name">House Name</label>
									<input type="text" name="house_name" value="" class="form-control" id="house_name" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="house_town">Town</label>
									<input type="text" name="house_town" class="form-control" id="house_town" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="house_estate">Estate</label>
									<input type="text" name="house_estate" class="form-control" id="house_estate" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="house_rent">Rent Amount</label>
									<input type="text" name="house_rent" class="form-control" id="house_rent" >
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="form-group">
	                                <label class="control-label" for="house_state">House State</label>
	                              	<div class="">
	                                  	<select name="house_state" class="form-control m-bot15">
	                                      	<option>Vacant</option>
	                                      	<option>Occupied</option>
	                                  	</select>
	                              	</div>
	                          	</div>
							</td>
							<td>
								<div class="form-group">
									<label for="house_photo">House Photo</label>
									<input name="house_photo" class="form-control" type="file" id="house_photo" >
								</div>
							</td>
							<td colspan="2">
								<div class="form-group">
									<label for="house_description">Description</label>
									<textarea name="house_description" class="form-control"></textarea>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<button type="submit" class="btn btn-primary">Add</button>
							</td>
						</tr>
					</tbody>
				</table>
				
			</form>
		</div>
	</section>
<?php } ?>

<?php if (@$_SESSION["editor"]!=""){?>

	<?php include('partials/edit_house.php'); ?>
	
<?php }elseif (@$_SESSION["delete_apart"]!="") { ?>

	<?php include('partials/processors/trash_house.php'); ?>

<?php } ?>

	<section class="panel">
		<header class="panel-heading">
			All Houses
		</header>
		<table class="table table-striped table-advance table-hover">
			<tbody>
				<tr>
					<th>#</th>
					<th>Photo</th>
					<th>House Name</th>
					<th>Town</th>
					<th>Estate</th>
					<th>Rent Amount</th>
					<th>House State</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
                <?php $i=1; $no=$page-1; $no=$no*$limit;

                    $result = $db->query("SELECT * FROM houses ORDER BY house_reg_date ");
                    while ($line = $db->fetchNextObject($result)) {

                ?>
				<tr>
					<th><?php echo $no+$i; ?>.</th>
					<td>
						<img src="../../../../public/assets/uploaded/<?php echo $line->house_photo ?>" class="img-list" />
					</td>
					<td><?php echo $line->house_name ?></td>
					<td><?php echo $line->house_town ?></td>
					<td><?php echo $line->house_estate ?></td>
					<td><?php echo $line->house_rent ?></td>
					<td><?php echo $line->house_state ?></td>
					<td><a href="#"><i class="fa fa-eye"></i> <?php echo $line->house_description ?></a></td>
					<td>
						<a class="btn btn-primary" href="config/setpage.php?editor=<?php echo $line->id_houses ?>" title="edit this house"><i class="fa fa-edit"></i></a>
						<a class="btn btn-danger" href="config/setpage.php?delete_apart=<?php echo $line->id_houses ?>" title="delete this house"><i class="icon_trash"></i></a>
					</td>
				</tr>
				<?php $i++;

                    }

                ?>             
			</tbody>
		</table>
	</section>