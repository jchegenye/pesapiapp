<?php ?>

<!--
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
-->
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<?php include('partials/sub_links.php'); ?>	
			</ol>
		</div>
	</div>

<section class="panel">
		<header class="panel-heading">
			All Apartments
		</header>

		<table class="table table-striped table-advance table-hover">
			<tbody>
				<tr>
					<th>#</th>
					<th>Name(s)</th>
					<th>Apartment Name</th>
					<th>Town</th>
					<th>Estate</th>
					<th>Description</th>
					<th>Parking</th>
					<th>Action</th>
				</tr>
	                <?php $i=1; $no=$page-1; $no=$no*$limit;

                        $result = $db->query("SELECT * FROM inquiries ORDER BY inquire_date ");
                        while ($line = $db->fetchNextObject($result)) {

                    ?> 
				<tr>
					<th><?php echo $no+$i; ?>.</th>
					<td>
						<?php echo $line->apartment_photo ?>" class="img-list" />
					</td>
					<td><?php echo $line->apartment_name ?></td>
					<td><?php echo $line->apartment_town ?></td>
					<td><?php echo $line->apartment_estate ?></td>
					<td><?php echo $line->apartment_description ?></td>
					<td><?php echo $line->apartment_parking ?></td>
					<td>
						<a class="btn btn-primary" href="config/setpage.php?edit_apart=<?php echo $line->apart_id ?>title="edit this apartment"><i class="fa fa-edit"></i></a>
						<a class="btn btn-success" href="config/setpage.php?view_tenants=1&apart=mzima" title="view tenants on this apartment"><i class="fa fa-eye"></i></a>
						<a class="btn btn-danger"  href="config/setpage.php?delete_apart=<?php echo $line->apart_id ?>" title="delete this apartment"><i class="icon_trash"></i></a>
					</td>
				</tr>
				<?php $i++;

                    }

                ?>              
			</tbody>
		</table>

	</section>