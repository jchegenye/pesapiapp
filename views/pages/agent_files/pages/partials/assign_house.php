	<section class="panel">
		<header class="panel-heading">
			Tenant
		</header>
		<div class="panel-body">
			<?php 

				$auto =$db->maxOfAll ("tenant_given_id","houses");
				$auto=$auto+1;
				$autoNo="H"."-".$auto."";

				$random_id = $_SESSION['editor'];

                $result = $db->query("SELECT * FROM tenants WHERE id_tenant = '$random_id' ");
                while ($line3 = $db->fetchNextObject($result)) {

            ?>
			<div class="col-md-12">
				<table>
					<tbody>
						<tr>
							<td colspan="1">
								<form method="post" action="config/processors/fetch_house.php" class="form-inline">
									<div class="form-group">
										<label for="take_house">Select house for <?php echo $line3->tenant_fname ?> <?php echo $line3->tenant_lname ?></label>
										<select name="take_house" onChange="this.form.submit()" class="form-control">
	                                  		<option value="">Non</option>
	              			                <?php
						                        $result = $db->query("SELECT * FROM houses WHERE house_state = 'Vacant' ");
						                        while ($line_house = $db->fetchNextObject($result)) {
	                						?>
	                                      	<option value="<?php echo $line_house->id_houses; ?>">
	                                      		<?php echo $line_house->house_name; ?>
	                                      		(<?php echo $line_house->house_town; ?>, <?php echo $line_house->house_estate; ?>)
	                                  		</option>
	                      					<?php } ?>
	                                  	</select>
									</div>
								</form>
							</td>
						</tr>
					</tbody>
				</table>
				<hr />
			</div>

			<div class="col-md-6">
				<form method="post" action="config/processors/update_assigned_house.php">
				<input type="hidden" name="random_id" value="<?php echo $_SESSION['id_houses'] ?>">
				<input type="hidden" name="tenant_given_id" value="<?php echo $line3->id_tenant ?>" >

				/*Select pesapi_payment table and compare it with this number*/
				<?php echo $line3->tenant_phone ?>

					<table>
						<tbody>
							<tr>
								<td rowspan="4" valign="top">
									<img src="../../../../../public/assets/uploaded/<?php echo $line3->tenant_photo; ?>" style="height: 130px;margin-right:15px; display: inline-block;" />
								</td>
								<td>
									<div class="form-group">
										<label for="u_tenant_id">Tenant ID</label>
										<input type="text" value="<?php echo $line3->tenant_id ?>" class="form-control" readonly="true" style="color: red;">
									</div>
								</td>
								<td>
									<div class="form-group">
										<label for="u_house">House</label>
										<input type="text" value="<?php echo $_SESSION['house_name'] ?>" class="form-control"readonly="true" style="color: red;">
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="form-group">
										<label for="lname">House Rent</label>
										<input type="text" value="<?php echo $_SESSION['house_rent'] ?>" class="form-control" readonly="true" style="color: red;">
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="form-group">
										<label for="house_given_no">House ID</label>
										<input type="text" name="house_given_no" value="<?php echo $autoNo ?>" class="form-control" readonly="true" style="color: red;">
									</div>
								</td>
								<td>
									<div class="form-group">
										<label for="house_given_date">Date</label>
										<input type="date" name="house_given_date" value="<?php echo date('Y-m-d'); ?>" class="form-control" id="house_id" >
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<button type="submit" class="btn btn-primary">Add</button>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

			<?php 

                }

            ?>

			<div class="col-md-6">
				<table class="table  table-advance table-hover">
					<tr><th colspan="6">Transactions</th></tr>
					<tbody>
						<tr>
							<th>#</th>
							<th>Tenant</th>
							<th>House</th>
							<th>House Cost</th>
							<th>Payment Status</th>
						</tr>
						<?php $i=1; $no=$page-1; $no=$no*$limit;
							/*Fetch Occupied houses Promary Key*/
							$result = $db->query("SELECT * FROM houses WHERE house_state = 'Occupied' ");
							while ($line = $db->fetchNextObject($result)) { 
								$get_id = $line->tenant_given_id;

							/*Compare the primary key between the two tables*/
							$result3 = $db->query("SELECT * FROM tenants WHERE id_tenant = '$get_id' ");
							while ($line3 = $db->fetchNextObject($result3)) {
						
						?>
						<tr>
							<td><?php echo $no+$i; ?>.</td>
							<td><?php echo $line3->tenant_id ?></td>
							<td><?php echo $line->tenant_given_id ?></td>
							<td>
								Rent to Pay: <?php echo $line->house_rent ?>
							</td>
							<td>pending</td>
						</tr> 
						<?php } } ?>             
					</tbody>
				</table>
			</div>
		</div>
	</section>