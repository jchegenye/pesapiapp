	<section class="panel">
		<header class="panel-heading">
			(Mzima Apartment) - Tenants
		</header>
		<table class="table table-striped table-advance table-hover">
			<tbody>
				<tr>
					<th>#</th>
					<th>Tenant Id</th>
					<th>Tenant Names</th>
					<th>Tenant PhoneNo.</th>
					<th>Houses</th>
					<th>Action</th>
				</tr>
				<tr>
					<td>1. </td>
					<td>pic</td>
					<td>Angeline</td>
					<td>2004</td>
					<td>Rosser</td>
					<td>
						<a class="btn btn-success" href="" title="view this tenants details"><i class="fa fa-eye"></i></a>
						<a class="btn btn-danger" href="" title="delete this tenant"><i class="icon_trash"></i></a>
					</td>
				</tr>              
			</tbody>
		</table>
	</section>