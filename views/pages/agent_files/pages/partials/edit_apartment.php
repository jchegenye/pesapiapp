<?php 
	
	$random_id = $_SESSION['edit_apart'];
	
    $result = $db->query("SELECT * FROM apartments WHERE  apart_id = '$random_id'");
    while ($line = $db->fetchNextObject($result)) {

    	if(isset($_GET['message'])){
	
		$data=$_GET['message'];
		$msg =''.$data.'';

		?>
			<div class="alert alert-success text-center"><?php echo  $msg; ?></div>
		<?php
	}

?>
<section class="panel">
	<header class="panel-heading">
		Edit <b>(<?php echo $line->apartment_name; ?>)</b>
	</header>
	<div class="panel-body">
		<form method="post" action="config/processors/update_apartment.php" enctype="multipart/form-data">
			<input type="hidden" name="apart_id" value="<?php echo $line->apart_id; ?>">
			<input type="hidden" name="apartment_reg_date" value="<?php echo $line->registered_date; ?>">
			<table>
				<tbody>
					<tr>
						<td rowspan="4" valign="top">
							<img src="../../../../../public/assets/uploaded/<?php echo $line->apartment_photo; ?>" style="height: 130px;margin-right:15px; display: inline-block;" />
						</td>
						<td>
							<div class="form-group">
								<label name="apartment_name" for="apartment_name">Apartment Name</label>
								<input type="text" name="apartment_name" value="<?php echo $line->apartment_name; ?>" class="form-control" id="names" >
							</div>
						</td>
						<td>
							<div class="form-group">
                                <label class="control-label " for="inputSuccess">Parking</label>
                              	<div class="">
                                  	<select name="apartment_parking" value="<?php echo $line->apartment_parking; ?>" class="form-control m-bot15">
                                      	<option>Yes</option>
                                      	<option>No</option>
                                  	</select>
                              	</div>
                          	</div>
						</td>
						<td>
							<div class="form-group">
								<label for="photo">Photo</label>
								<input name="apartment_photo" value="<?php echo $line->apartment_photo ?>" class="form-control" type="file" id="photo" >
							</div>
						</td>
						
					</tr>
					<tr>
						<td>
							<div class="form-group">
								<label for="apartment_town">Town</label>
								<input type="text" name="apartment_town" value="<?php echo $line->apartment_town ?>" class="form-control" id="apartment_town" >
							</div>
						</td>
						<td>
							<div class="form-group">
								<label for="apartment_estate">Estate</label>
								<input type="text" name="apartment_estate" value="<?php echo $line->apartment_estate ?>" class="form-control" id="apartment_estate" >
							</div>
						</td>
						<td>
							<div class="form-group">
								<label for="apartment_description">Description</label>
								<textarea name="apartment_description" class="form-control" placeholder="<?php echo $line->apartment_description ?>"></textarea>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<button type="submit" class="btn btn-primary">Edit</button>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</section>
<?php $i++;

    }

?> 