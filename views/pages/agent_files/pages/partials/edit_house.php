<?php 
	//Change this to autoselect number of house units in an apartment
	$gen_code =$db->maxOfAll ("id_houses","houses");
	$gen_code= $gen_code+1;
	$code="A".$gen_code;

	$random_id = $_SESSION['editor'];
	
    $result = $db->query("SELECT * FROM houses WHERE id_houses = '$random_id'");
    while ($line2 = $db->fetchNextObject($result)) {

    	if(isset($_GET['edit_message'])){
	
			$data=$_GET['edit_message'];
			$msg =''.$data.'';

			?>
				<div class="alert alert-success text-center"><?php echo  $msg; ?></div>
			<?php
		}

?>
	<section class="panel">
		<header class="panel-heading">
			Edit house <?php echo $line2->house_id ?>
		</header>
		<div class="panel-body">
			<form method="post" action="config/processors/update_house.php" enctype="multipart/form-data">
				<input type="hidden" name="registered_date" value="<?php echo date('Y-m-d'); ?>">
				<input type="hidden" name="id" value="<?php echo $line2->id_houses; ?>">
				<table>
					<tbody>
						<tr>
							<td rowspan="4" valign="top">
								<img src="../../../../../public/assets/uploaded/<?php echo $line2->house_photo; ?>" style="height: 130px;margin-right:15px; display: inline-block;" />
							</td>
							<td>
								<div class="form-group">
									<label for="house_name">House Name</label>
									<input type="text" name="house_name" value="<?php echo $line2->house_name; ?>" class="form-control" id="house_name" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="house_town">Town</label>
									<input type="text" name="house_town" value="<?php echo $line2->house_town; ?>" class="form-control" id="house_town" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="house_estate">Estate</label>
									<input type="text" name="house_estate" value="<?php echo $line2->house_estate; ?>" class="form-control" id="house_estate" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="house_rent">Rent Amount</label>
									<input type="text" name="house_rent" value="<?php echo $line2->house_rent; ?>" class="form-control" id="house_rent" >
								</div>
							</td>
						</tr>
						<tr>
	                         <td>
								<div class="form-group">
	                                <label class="control-label" value="<?php echo $line2->house_state; ?>"  for="house_state">House State</label>
	                              	<div class="">
	                                  	<select name="house_state" class="form-control m-bot15">
	                                      	<option>Vacant</option>
	                                      	<option>Occupied</option>
	                                  	</select>
	                              	</div>
	                          	</div>
							</td>
							<td>
								<div class="form-group">
									<label for="house_photo">House Photo</label>
									<input name="house_photo" value="<?php echo $line2->house_photo; ?>" class="form-control" type="file" id="house_photo" >
								</div>
							</td>
							<td colspan="2">
								<div class="form-group">
									<label for="house_description">Description</label>
									<textarea name="house_description" placeholder="<?php echo $line2->house_description; ?>" class="form-control"></textarea>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<button type="submit" class="btn btn-primary">Edit</button>
							</td>
						</tr>
					</tbody>
				</table>
				
			</form>
		</div>
	</section>

<?php 

    }

?> 