<?php ?>

<!--
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
-->
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<?php include('partials/sub_links.php'); ?>	
			</ol>
		</div>
	</div>

<?php if ($_SESSION["view_tenants"] == "" && $_SESSION["edit_apart"] == "" && $_SESSION["delete_apart"] == ""){

  	if(isset($_GET['message'])){
	
		$data=$_GET['message'];
		$msg =''.$data.'';

		?>
			<div class="alert alert-success text-center"><?php echo  $msg; ?></div>
		<?php
	}

?>
	

	<section class="panel">
		<header class="panel-heading">
			Add Apartment 
		</header>

		<div class="panel-body">
			<form method="post" action="config/processors/addapartment.php" enctype="multipart/form-data">
				<input type="hidden" name="apartment_reg_date" value="<?php echo date('Y-m-d'); ?>">
				<!--input type="text" name="apartment_id" value=""-->
				<table>
					<tbody>
						<tr>
							<td>
								<div class="form-group">
									<label for="apartment_name">Apartment Name</label>
									<input type="text" name="apartment_name" class="form-control" id="apartment_name" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="apartment_town">Town</label>
									<input type="text" name="apartment_town" class="form-control" id="apartment_town" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="apartment_estate">Estate</label>
									<input type="text" name="apartment_estate" class="form-control" id="apartment_estate" >
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="form-group">
									<label for="apartment_description">Description</label>
									<textarea name="apartment_description" class="form-control" placeholder=""></textarea>
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="apartment_photo">Photo</label>
									<input name="apartment_photo" class="form-control" type="file" id="apartment_photo" />
								</div>
							</td>
							<td>
								<div class="form-group">
	                                <label class="control-label" for="apartment_parking">Parking</label>
	                              	<div class="">
	                                  	<select name="apartment_parking" class="form-control">
	                                      	<option>Yes</option>
	                                      	<option>No</option>
	                                  	</select>
	                              	</div>
	                          	</div>
							</td>
						</tr>
						<tr>
							<td>
								<button type="submit" class="btn btn-primary">Add</button>
							</td>
						</tr>
					</tbody>
				</table>
				
			</form>
		</div>
	</section>
<?php } elseif ($_SESSION["edit_apart"]){ ?>

	<?php include('partials/edit_apartment.php'); ?>

<?php } elseif ($_SESSION["view_tenants"]){?>

	<?php include('partials/tenants.php'); ?>

<?php } elseif ($_SESSION["delete_apart"]){ ?>
	
	<?php include('partials/processors/trash_apartment.php'); ?>

<?php } ?>


	<section class="panel">
		<header class="panel-heading">
			All Apartments
		</header>

		<table class="table table-striped table-advance table-hover">
			<tbody>
				<tr>
					<th>#</th>
					<th>Photo</th>
					<th>Apartment Name</th>
					<th>Town</th>
					<th>Estate</th>
					<th>Description</th>
					<th>Parking</th>
					<th>Action</th>
				</tr>
	                <?php $i=1; $no=$page-1; $no=$no*$limit;

                        $result = $db->query("SELECT * FROM apartments ORDER BY apartment_reg_date ");
                        while ($line = $db->fetchNextObject($result)) {

                    ?> 
				<tr>
					<th><?php echo $no+$i; ?>.</th>
					<td>
						<img src="../../../../../public/assets/uploaded/<?php echo $line->apartment_photo ?>" class="img-list" />
					</td>
					<td><?php echo $line->apartment_name ?></td>
					<td><?php echo $line->apartment_town ?></td>
					<td><?php echo $line->apartment_estate ?></td>
					<td><?php echo $line->apartment_description ?></td>
					<td><?php echo $line->apartment_parking ?></td>
					<td>
						<a class="btn btn-primary" href="config/setpage.php?edit_apart=<?php echo $line->apart_id ?>title="edit this apartment"><i class="fa fa-edit"></i></a>
						<a class="btn btn-success" href="config/setpage.php?view_tenants=1&apart=mzima" title="view tenants on this apartment"><i class="fa fa-eye"></i></a>
						<a class="btn btn-danger"  href="config/setpage.php?delete_apart=<?php echo $line->apart_id ?>" title="delete this apartment"><i class="icon_trash"></i></a>
					</td>
				</tr>
				<?php $i++;

                    }

                ?>              
			</tbody>
		</table>

	</section>
