<?php ?>

<!--
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
-->
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<?php include('partials/sub_links.php'); ?>	
			</ol>
		</div>
	</div>

<?php if ($_SESSION["editor"] == ""){?>
	<section class="panel">
		<header class="panel-heading">
			Add Tenant
		</header>
		<div class="panel-body">
			<form method="post" action="config/processors/addtenant.php" enctype="multipart/form-data">
				<input type="hidden" name="tenant_reg_date" value="<?php echo date('Y-m-d'); ?>">
				<input type="hidden" name="usertype" value="tenant">
				<input type="hidden" name="count_approval" value="1">
				<input type="hidden" name="password" value="123">
				<table>
					<tbody>
						<tr>
							<td>
								<div class="form-group">
									<label for="tenant_fname">First Name</label>
									<input type="text" name="tenant_fname" class="form-control" id="tenant_fname" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="tenant_lname">Last Name</label>
									<input type="text" name="tenant_lname" class="form-control" id="tenant_lname" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="tenant_email">Email</label>
									<input type="text" name="tenant_email" class="form-control" id="tenant_email" >
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="form-group">
									<label for="tenant_photo">Photo</label>
									<input name="tenant_photo" class="form-control" type="file" id="tenant_photo" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="tenant_phone">Phone No.</label>
									<input type="text" name="tenant_phone" class="form-control" id="tenant_phone" >
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="tenant_id">Identity No.</label>
									<input type="text" name="tenant_id" class="form-control" id="tenant_id" >
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<button type="submit" class="btn btn-primary">Add</button>
							</td>
						</tr>
					</tbody>
				</table>
				
			</form>
		</div>
	</section>
<?php } ?>

<?php if (@$_SESSION["editor"]!=""){ ?>

	<?php include('partials/assign_house.php'); ?>
	
<?php } ?>

	<section class="panel">
		<header class="panel-heading">
			All Tenants
		</header>
		<table class="table table-striped table-advance table-hover">
			
			<tbody>
				<tr>
					<th>#</th>
					<th>Photo</th>
					<th>ID</th>
					<th>Names</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Action</th>
				</tr>
                <?php $i=1; $no=$page-1; $no=$no*$limit;

                    $result = $db->query("SELECT * FROM tenants ORDER BY tenant_reg_date ");
                    while ($line = $db->fetchNextObject($result)) {

                ?>
				<tr>
					<th><?php echo $no+$i; ?>.</th>
					<td>
						<img src="../../../../public/assets/uploaded/<?php echo $line->tenant_photo ?>" class="img-list" />
					</td>
					<td><?php echo $line->tenant_id ?></td>
					<td><?php echo $line->tenant_fname ?> <?php echo $line->tenant_lname ?></td>
					<td><?php echo $line->tenant_email ?></td>
					<td><?php echo $line->tenant_phone ?></td>
					<td>
						<a class="btn btn-default" href="config/setpage.php?editor=<?php echo $line->id_tenant ?>" title="Assign house"><i class="fa fa-eye"></i></a>
						<a class="btn btn-primary" href="#" title="delete this house"><i class="fa fa-money"></i></a>
						<!-- <a class="btn btn-primary" href="#" title="edit this house"><i class="fa fa-edit"></i></a>
						<a class="btn btn-danger" href="#" title="delete this house"><i class="icon_trash"></i></a> -->
					</td>
				</tr>
				<?php $i++;

                    }

                ?>               
			</tbody>
		</table>
	</section>