<?php 

@$from=$_SESSION['from_date'];
@$to=$_SESSION['to_date'];
@$apartments=$_SESSION['apartments'];

?>

<!--
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
-->
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="config/setpage.php?page=dashboard.php">Home</a></li>					  	
			</ol>
		</div>
	</div>

	<section class="panel">
		<header class="panel-heading">
			Income Report
		</header>
		
		<div class="panel-body">
		<p>
			Select Range of Time within which to report ...
		</p>
			<table>
				<tbody>
					<form method="post" action="code/addapartment.php" enctype="multipart/form-data">
						<tr>
							<td>
								<div class="form-group">
		                            <label class="control-label " for="apartment"><b>Apartment</b></label>
		                          	<select name="apartments" class="form-control m-bot15">
		                              	<option>Non</option>
		                          	</select>
		                      	</div>
							</td>
							<td> &nbsp &nbsp&nbsp</td>
							<td>
								<div class="form-group">
									<label for="code"><b>From:</b></label>
									<input type="date" name="from_date" class="form-control" id="code" style="cursor:pointer;">
								</div>
							</td>
							<td>
								<div class="form-group">
									<label for="rent"><b>To:</b></label>
									<input type="date" name="to_date" class="form-control" id="rent" style="cursor:pointer;">
								</div>
							</td>
							<td> &nbsp &nbsp&nbsp</td>
							<td>
								<button type="submit" class="btn btn-success" style="margin-top: 6px;">Add</button>
							</td>
						</tr>
					</form>
				</tbody>
			</table>

			Sorted List of apartment (<?php echo $_SESSION['apartments']; ?>) 
			from <em style="color:blue;"><?php echo @$_SESSION['from_date'];?></em> 
			to <em style="color:blue;"><?php echo @$_SESSION['to_date'];?></em>
		</div>
	</section>

	<section class="panel">
		<div class="panel-body">
			<div id="report">
			<table class="table table-advance report-table">
				<tbody>
					<tr>
						<td colspan="5" style="border-top: 0px solid;">
							<h3>Online Estate Managment System</h3>
							<em>We care for you!</em>
							<p style="padding-top: inherit;"><?php echo date('M, d Y'); ?></p>
						</td>
						<td style="border-top: 0px solid;" class="text-right">
							
								P.O Box 025 - 50100<br>
								Nairobi,<br>
								Kenya.
							
						</td>
					</tr>
					<tr>
						<th>#</th>
						<th>Description</th>
						<th>HouseNo.</th>
						<th>Tenant Name</th>
						<th>Amount</th>
						<th>Date</th>
					</tr>
					<tr>
						<td>1. </td>
						<td>pic</td>
						<td>Angeline</td>
						<td>2004</td>
						<td>2004</td>
						<td>Rosser</td>
					</tr> 
					<tr>
						<th colspan="4" class="text-right">
							Total:
						</th>
						<th style="border-bottom: 5px solid; border-top: 5px solid;">
							5000 /= 
						</th>
					</tr>             
				</tbody>
			</table>
			</div>
			<input type="button" class="btn btn-primary" value="Print Report" onClick="printDiv('report')"/>
		</div>

	</section>