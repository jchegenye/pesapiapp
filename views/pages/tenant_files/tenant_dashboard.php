<?php session_start();
/**
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/

    include("header.php");

?>

<div class="container">

	<section class="panel">
          <header class="panel-heading tab-bg-primary ">
              <ul class="nav nav-tabs">
                  <li class="active">
                      <a data-toggle="tab" href="#home">Mpesa Transactions</a>
                  </li>
                  <li class="">
                      <a data-toggle="tab" href="#about">Payments</a>
                  </li>
              </ul>
          </header>
          <div class="panel-body">
              <div class="tab-content">
                  <div id="home" class="tab-pane active">

<table class="table table-striped table-advance table-hover">
			
			<tbody>
				<tr>
					<th>#</th>
					<th>Receipt</th>
					<th>Time</th>
					<th>Names</th>
					<th>Account</th>
					<th>Amount</th>
					<th>Transaction Cost</th>
				</tr>
                <?php $i=1; $no=$page-1; $no=$no*$limit;

                    $result = $db->query("SELECT * FROM pesapi_payment WHERE phonenumber = ".$ADMINS['phonenumber']." ORDER BY time ");
                    while ($line = $db->fetchNextObject($result)) {

                ?>
				<tr>
					<th><?php echo $no+$i; ?>.</th>
					<td>
						<?php echo $line->receipt ?>
					</td>
					<td><?php echo $line->time ?></td>
					<td><?php echo $line->account ?> <?php echo $line->status ?></td>
					<td><?php echo $line->amount ?><?php echo $line->post_balance ?></td>
					<td><?php echo $line->tenant_phone ?></td>
					<td>
						<?php echo $line->transaction_cost ?>
					</td>
				</tr>
				<?php $i++;

                    }

                ?>               
			</tbody>
		</table>

                  </div>
                  <div id="about" class="tab-pane">
                  
                  	<table class="table  table-advance table-hover">
						<tr><th colspan="6">Transactions</th></tr>
						<tbody>
							<tr>
								<th>#</th>
								<th>Tenant</th>
								<th>House</th>
								<th>House Cost</th>
								<th>Payment Status</th>
							</tr>
							<?php $i=1; $no=$page-1; $no=$no*$limit;
								/*Fetch Occupied houses Promary Key*/
								$result = $db->query("SELECT * FROM houses WHERE house_state = 'Occupied' ");
								while ($line = $db->fetchNextObject($result)) { 
									$get_id = $line->tenant_given_id;

								/*Compare the primary key between the two tables*/
								$result3 = $db->query("SELECT * FROM tenants WHERE id_tenant = '$get_id' ");
								while ($line3 = $db->fetchNextObject($result3)) {
							
							?>
							<tr>
								<td><?php echo $no+$i; ?>.</td>
								<td><?php echo $line3->tenant_id ?></td>
								<td><?php echo $line->tenant_given_id ?></td>
								<td>
									Rent to Pay: <?php echo $line->house_rent ?>
								</td>
								<td>pending</td>
							</tr> 
							<?php } } ?>             
						</tbody>
					</table>
                  </div>
              </div>
          </div>
      </section>
	
	<div class="panel" style="margin-top: 15px;">
	  	<div class="panel-body">
			<i class="fa fa-copyright"> 2016 Copyright | Developed By <a href="#">Chegenye</a> 
	  	</div>
	</div>
</div>


