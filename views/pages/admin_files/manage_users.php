<?php
/**
    * @Author: Jackson Chegenye
    * @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
    * @Version: 0.0.5v
    * @Location: Nairobi - Kenya
    * @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/

    include("header.php");

	if(isset($_GET['msg'])){
	
		$data=$_GET['msg'];
		$msg=''.$data.'';

	}

?>

<div class="container-fluid">

	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-users"></i>
			Manage Users
		</h3>
            <?php if(isset($_GET['msg'])){
            
                $data=$_GET['msg'];
                $msg =''.$data.'';

                    ?>
                        <div class="alert alert-danger text-center">
                            <?php echo $msg; ?> 
                        </div>
                    <?php
            }
            ?>
	</div>

  <div class="col-lg-12">
      <section class="panel">
          
        <table class="table table-striped table-advance table-hover">
            <tbody>
                <tr>   
                    <th>#</th>
                    <th><i class="icon_profile"></i> Full Name</th>
                    <th><i class="icon_calendar"></i> Registered Date</th>
                    <th><i class="icon_mobile"></i> Mobile Number</th>
                    <th><i class="fa fa-th-list"></i> User Type</th>
                    <th><i class="icon_cogs"></i> Action</th>
                </tr>

                <?php $i=1; $no=$page-1; $no=$no*$limit;

                    $SQL = "SELECT * FROM users ORDER BY id_user ";
                    $result = mysql_query($SQL);    
                        while ($db_field = mysql_fetch_assoc($result)) {
                            
                            $uid = $db_field['id_user'];
                            $names = $db_field['names'];
                            $registered_date = $db_field['registered_date'];
                            $phonenumber = $db_field['phonenumber'];
                            $usertype = $db_field['usertype']; 

                            $count_approval = $db_field['count_approval'];

                            mysql_close($db_handle);    

                        { 
                    ?> 

                <tr>
                    <th><?php echo $no+$i; ?></th>
                    <td><?php echo $names ?></td>
                    <td><?php echo $registered_date ?></td>
                    <td><?php echo $phonenumber ?></td>
                    <td><?php echo $usertype ?></td>
                    <td>
                        <div class="btn-group">

                            <?php if(!$count_approval == '0'){ ?>
                                <a class="btn btn-success" href="processors/approve_user_processor.php?uid=<?php echo $uid ?>&count_approval=<?php echo $count_approval ?>" title="Approved"><i class="icon_check_alt2"></i></a>
                            <?php }else{ ?>
                                <a class="btn btn-danger" href="processors/approve_user_processor.php?uid=<?php echo $uid ?>&count_approval=<?php echo $count_approval ?>" title="Declined"><i class="icon_close_alt2"></i></a>
                            <?php } ?>
                                <a class="btn btn-default" href="edit_user.php?uid=<?php echo $uid ?>"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger" href="processors/trash_user_processor.php?uid=<?php echo $uid ?>&type=<?php echo $usertype ?>"><i class="icon_trash"></i></a>
                        </div>
                    </td>
                </tr>

                <?php $i++;

                    }

                } ?>
                           
           </tbody>
        </table>
      </section>
  </div>

</div>