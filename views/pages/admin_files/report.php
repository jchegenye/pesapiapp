<?php
/**
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/

    include("header.php");

	if(isset($_GET['msg'])){
	
		$data=$_GET['msg'];
		$msg=''.$data.'';

	}

	$autopass =$db->maxOfAll ("id_user","users");
	$autopass=$autopass+1;
	$newautopass="password"."-".$autopass."-".date(Y);	

?>

<div class="container-fluid">

	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-flag"></i>
			Users Report
		</h3>
	</div>


    	<div  class="col-md-12">

			<form action="report.php" method="GET">

				<section class="panel">
                      	<div class="panel-body">
                      		<h6>Select date:</h6>
								<div class="input-group">
						          	<span class="input-group-addon"><i class="icon_calendar"></i> From: </span>
						          	<input type="date" name="from_this_date" class="form-control" autofocus>

						          	<span class="input-group-addon"><i class="icon_calendar"></i> To: </span>
						          	<input type="date" name="to_this_date" class="form-control" autofocus>
						        </div>
							<h6>Select user type:</h6>
								<div class="input-group">
						          	<span class="input-group-addon">
						          		<input type="radio" name="usertype" value="admin"> Administrator	 
					          			&nbsp | &nbsp
						          		<input type="radio" name="usertype" value="guest"> Guest
					          		</span>&nbsp &nbsp
						        </div>
					        <h6>Select approval/decline:</h6>
								<div class="input-group">
						          	<span class="input-group-addon">
						          		<i class="icon_check_alt2"></i>&nbsp &nbsp
						          		<input type="radio" name="app_decl" value="1"> Approved	 
					          			&nbsp | &nbsp
						          		<i class="icon_close_alt2"></i>&nbsp &nbsp
						          		<input type="radio" name="app_decl" value="0"> Declined
					          		</span>&nbsp &nbsp
						        </div>
						       	<br>
						      	<button class="btn btn-info btn-lg btn-block" type="submit">Go</button>

                      	</div>
                  </section>

			</form>

			<?php if(
				isset($_GET['from_this_date']) && 
				isset($_GET['to_this_date']) && 
				isset($_GET['app_decl']) && 
				isset($_GET['usertype'])

				&& $_GET['from_this_date']!='' 
				&& $_GET['to_this_date']!='' 
				&& $_GET['app_decl']!=''
				&& $_GET['usertype']!=''
				)
			{
				error_reporting (E_ALL ^ E_NOTICE);
					$selected_date=$_GET['from_this_date'];
				  	$selected_date=strtotime( $selected_date );
					$mysqldate = date( 'Y-m-d H:i:s', $selected_date );
				$fromdate=$mysqldate;
					$selected_date=$_GET['to_this_date'];
				  	$selected_date=strtotime( $selected_date );
					$mysqldate = date( 'Y-m-d H:i:s', $selected_date );
				$todate=$mysqldate;

			?>

		<section class="panel">
          	<div class="panel-body">

				<table width="100%">
					<tr>
						<td class="alert alert-success text-center" colspan="5">
						The following report shows the total number of users only, depending on what you selected!	
						</td>
					</tr>
					<tr><td colspan="5"> <hr> </td></tr>
		        	<tr>
		          	<td width="45"><font color="black"><strong>From</strong> |</font></td>
		          	<td width="393">&nbsp;<?php echo $_GET['from_this_date']; ?></td>
		          	<td width="41"><font color="black"><strong>To</strong> |</font></td>
		          	<td width="100">&nbsp;<?php echo $_GET['to_this_date']; ?></td>
		        	</tr>
		        	<tr><td colspan="5"> <hr> </td></tr>
		       	</table>

       			<table width="100%">
					<tr>
		    			<th><i class="icon_profile"></i> Full Name</th>
		             	<th><i class="icon_calendar"></i> Registered Date</th>
		             	<th><i class="icon_mobile"></i> Mobile Number</th>
		             	<th><i class="fa fa-th-list"></i> User Type</th>
					</tr>
	
					<?php 
						$result = $db->query("SELECT * FROM users WHERE 
							count_approval = '".$_GET['app_decl']."' AND 
							usertype = '".$_GET['usertype']."' AND 
							registered_date BETWEEN '$fromdate' AND '$todate' ");

						while ($line = $db->fetchNextObject($result)) {
					?>
						<tr>
							<td style="border-bottom:1px solid #f8f9fa;">
								<font color="green"><?php echo $line->names; ?></font>
							</td>
							<td style="border-bottom:1px solid #f8f9fa;">
								<font color="green"><?php echo $line->registered_date; ?></font>
							</td>
							<td style="border-bottom:1px solid #f8f9fa;">
								<font color="green"><?php echo $line->phonenumber; ?></font>
							</td>
							<td style="border-bottom:1px solid #f8f9fa;">
								<font color="green"><?php echo $line->usertype; ?></font>
							</td>
						</tr>
						<tr><td colspan="5"> &nbsp </td></tr>
						<?php }  
							} else echo "<h4 align='center' class='alert alert-danger'>Table will be generated here once you have selected dates and usertype above</h4>";
						?>
				</table>	
			</div>
		</section>
		
	</div>

</div>