	<?php
/**
    * @Author: Jackson Chegenye
    * @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
    * @Version: 0.0.5v
    * @Location: Nairobi - Kenya
    * @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/ 
    include_once("init_admin.php");
    include "../../dbconnector.php";

  ?>
  <title>Admin's Dashboard</title>
  
  <?php include("../../partials/css-links.php"); ?>
  <?php include("../../partials/js-links.php"); ?>

    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="admins_dashboard.php">OEMS</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active">
                  <a href="#" data-toggle="dropdown"><i class="fa fa-users"></i> Users</a>
                    <ul class="dropdown-menu">
                        <li><a href="manage_users.php">Manage Users</a></li>
                        <li><a href="add_user.php" >Add User</a></li>
                    </ul>
                </li>
                <li><a href="report.php"><i class="fa fa-flag-o"></i> Report<span class="sr-only">(current)</span></a></li>
                <li> &nbsp </li>
                <li class="active">
                  <a href="#" data-toggle="dropdown"><i class="fa fa-money"></i> Payments</a>
                    <ul class="dropdown-menu">
                        <li><a href="mpesa_payments.php">M-pesa payments</a></li>
                        <!-- <li><a href="paypal.php">PayPal payments</a></li> -->
                    </ul>
                </li>
                <li> <a href="admins_dashboard.php">Statistics</a> </li>
                <li class="active">
                  <a href="#" data-toggle="dropdown"><i class="fa fa-cogs"></i> Settings</a>
                    <ul class="dropdown-menu">
                        <li><a href="../../../Api/pesaPi-master/php/webroot/index.php">M-pesa</a></li>
                        <!-- <li><a href="paypal.php">PayPal</a></li> -->
                    </ul>
                </li>
            </ul>
         
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <p class="alert alert-info welcome">
                        Welcome <?php echo $ADMINS['usertype'] ?>, 
                        <?php echo $ADMINS['phonenumber'] ?>!
                    </p>
                </li>
                <li>
                    <a href="../../logout.php" class="btn btn-danger" style="color: #FFF; background-color: #FF2D55; border: 0px solid;">Logout<span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
        
      </div>
    </nav>