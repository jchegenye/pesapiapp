<?php
/**
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/

    include("header.php");

	if(isset($_GET['msg'])){
	
		$data=$_GET['msg'];
		$msg=''.$data.'';

	}

	//Count all users
	$countAllUsers =$db->countOfAll("users");

	//Count approved & declined users only
	$countGuests =$db->countApprovedeDeclined("users", "count_approval", "count_approval");

?>

<div class="container-fluid">

	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="icon_piechart"></i>
			Statistics 
		</h3>
	</div>

	<div class="col-md-4">
		<div class="social-box google-plus">
			<i>Users</i>
			<?php echo $countAllUsers ?>
			<ul>
				<li>
					<strong><?php echo $countGuests ?></strong>
					<span>guests</span>
				</li>
				<li>
					<strong><?php echo $all_users_declined ?></strong>
					<span>others</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-md-4">
		<div class="social-box twitter">
			<i>Payments</i>
			<ul>
				<li>
					<strong>962</strong>
					<span>claimed</span>
				</li>
				<li>
					<strong>256</strong>
					<span>un-claimed</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-md-4">
		<div class="social-box facebook">
			<i> </i>
			<ul>
				<li>
					<strong></strong>
					<span></span>
				</li>
				<li>
					<strong></strong>
					<span></span>
				</li>
			</ul>
		</div>
	</div>

</div>