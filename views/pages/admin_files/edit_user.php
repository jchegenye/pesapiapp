<?php
/**
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/

    include("header.php");

  	$user_id = $_REQUEST['uid'];

  	$result = $db->query("SELECT * FROM users WHERE id_user = '$user_id' ");
  	while ($line = $db->fetchNextObject($result)) {

?>

<div class="container-fluid">

	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-edit"></i>
			Edit User <hr>
		</h3>
	</div>

	<form class="login-form" action="processors/edit_user_processor.php?uid=<?php echo $line->id_user ?>&type=<?php echo $line->usertype ?>" method="POST">      
	    <div class="login-wrap">
	        <p class="login-img"><i class="icon_lock_alt"></i></p>
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_profile"></i></span>
	          <input type="text" name="names" class="form-control" value="<?php echo $line->names ?>" autofocus>
	        </div>
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_phone"></i></span>
	          <input type="text" name="phonenumber" class="form-control" value="<?php echo $line->phonenumber ?>" autofocus>
	        </div>
	        <div class="input-group">
	            <span class="input-group-addon"><i class="fa fa-paper-plane"></i></span>
	            <select class="form-control" name="usertype" value="<?php echo $line->usertype ?>">
	            	<option>guest</option>
	            	<option>admin</option>
	            	<option>agent</option>
	            </select>
	        </div>
	        <button class="btn btn-info btn-lg btn-block" type="submit">Edit</button>
	    </div>
	  </form>

</div>

<?php } ?>