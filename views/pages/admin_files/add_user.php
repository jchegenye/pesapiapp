<?php
/**
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/

    include("header.php");

	if(isset($_GET['msg'])){
	
		$data=$_GET['msg'];
		$msg=''.$data.'';

	}

	$autopass =$db->maxOfAll ("id_user","users");
	$autopass=$autopass+1;
	$newautopass="password"."-".$autopass."-".date(Y);	

?>

<div class="container-fluid">

	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-user"></i>
			Add User <hr>
		</h3>
	</div>

	<form onsubmit="return checkForm(this);" class="login-form" action="processors/signup_processor2.php" method="POST">
			      
	    <div class="login-wrap">
	        <p class="login-img"><i class="icon_lock_alt"></i></p>
	        <?php

				if(isset($_GET['msg'])){
				
					$data=$_GET['msg'];
					$msg =''.$data.'';

						?>
							<div class="alert alert-danger text-center">
								<?php echo $msg; ?>	
							</div>
						<?php
				}

			?> 
	        <input type="hidden" name="count_approval" value="1">
	        <input type="hidden" name="registered_date" value="<?php echo date('Y-m-d'); ?>">
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_profile"></i></span>
	          <input type="text" name="names" class="form-control" placeholder="Full Names" autofocus>
	        </div>
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_phone"></i></span>
	          <input type="text" name="phonenumber" class="form-control" placeholder="Phone Number" autofocus>
	        </div>
	        <i>Auto generated password.</i>
	        <div class="input-group">
	            <span class="input-group-addon"><i class="icon_key_alt"></i></span>
	            <input type="text" name="password" class="form-control" value="<?php echo $newautopass ?>" disabled="true">
	        </div>
	        <div class="input-group">
	            <span class="input-group-addon"><i class="fa fa-paper-plane"></i></span>
	            <select class="form-control" name="usertype">
	            	<option>guest</option>
	            	<option>admin</option>
	            	<option>agent</option>
	            </select>
	        </div>
	        <button class="btn btn-info btn-lg btn-block" type="submit">Signup</button>
	    </div>
	  </form>

</div>