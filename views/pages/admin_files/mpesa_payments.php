<?php
/**
    * @Author: Jackson Chegenye
    * @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
    * @Version: 0.0.5v
    * @Location: Nairobi - Kenya
    * @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/

    include("header.php");

?>

<div class="container-fluid">

	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-money"></i>
			M-pesa Payments
		</h3>
            <?php if(isset($_GET['msg'])){
            
                $data=$_GET['msg'];
                $msg =''.$data.'';

                    ?>
                        <div class="alert alert-danger text-center">
                            <?php echo $msg; ?> 
                        </div>
                    <?php
            }
            ?>
	</div>

  <div class="col-lg-12">
      <section class="panel">
          
        <table class="table table-striped table-advance table-hover">
            <tbody>
                <tr>   
                    <th>#</th>
                    <th><i class="icon_profile"></i> Acc Id</th>
                    <th><i class=""></i> Super Type</th>
                    <th><i class=""></i> Type</th>
                    <th><i class=""></i> Receipt</th>
                    <th><i class="icon_calendar"></i> Time</th>
                    <th><i class=""></i> Phone No.</th>
                    <th><i class=""></i> Name</th>
                    <th><i class=""></i> Account</th>
                    <th><i class=""></i> Status</th>
                    <th><i class=""></i> Amount</th>
                    <th><i class=""></i> Post Balance</th>
                    <th><i class=""></i> Note</th>
                    <th><i class=""></i> Transaction Cost</th>
                    <th><i class=""></i> Action</th>
                </tr>

                <?php $i=1; $no=$page-1; $no=$no*$limit;

                        $result = $db->query("SELECT * FROM pesapi_payment ORDER BY time ");
                         while ($line = $db->fetchNextObject($result)) {

                    ?> 

                <tr>
                    <th><?php echo $no+$i; ?></th>
                    <td><?php echo $line->account_id ?></td>
                    <td><?php echo $line->super_type ?></td>
                    <td><?php echo $line->type ?></td>
                    <td><?php echo $line->receipt ?></td>
                    <td><?php echo $line->time ?></td>
                    <td><?php echo $line->phonenumber ?></td>
                    <td><?php echo $line->name ?></td>
                    <td><?php echo $line->account ?></td>
                    <td><?php echo $line->status ?></td>
                    <td><?php echo $line->amount ?></td>
                    <td><?php echo $line->post_balance ?></td>
                    <td><?php echo $line->note ?></td>
                    <td><?php echo $line->transaction_cost ?></td>
                    <td>
                        <div class="btn-group">
                            <a class="btn btn-default" href="edit_user.php?uid=<?php echo $line->id ?>">
                                <i class="fa fa-edit"></i>
                            </a>
                        </div>
                    </td>
                </tr>

                <?php $i++;

                    }

                ?>
                           
           </tbody>
        </table>
      </section>
  </div>

</div>