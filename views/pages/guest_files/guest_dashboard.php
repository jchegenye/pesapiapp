<?php
/**
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/

    include_once("init_guest.php");
    include "../../dbconnector.php";

	if(isset($_GET['msg'])){
	
		$data=$_GET['msg'];
		$msg=''.$data.'';
	
	?>
		<div class="container">
			<div  class="col-md-4"></div>
			<div  class="col-md-4"></div>
			<div class="col-md-4 alert alert-info text-center">
				<?php echo  $msg; ?> <?php echo $ADMINS['usertype'] ?>,  <?php echo $ADMINS['phonenumber'] ?>!
			</div>
		</div>
	<?php

	}

?>