
<?php
/**
  	* @Author: Jackson Chegenye
  	* @Contacts: +254 711 494 289 <chegenyejackson@gmail.com>
  	* @Version: 0.0.5v
  	* @Location: Nairobi - Kenya
  	* @Company: J-Tech Company Ke <http://www.j-tech.tech>
*/

session_start();

	include("../../public/lib/db.class.php");
	include_once "../dbconnector.php"; 

	$db = new DB($config['database'], $config['host'], $config['username'], $config['password']);
	$tbl_name = "users"; // Table name

	// Lets get the login data
	$myusername=$_REQUEST['phonenumber']; 
	$mypassword = md5($_REQUEST['password']); 

	// To protect MySQL injection (more detail about MySQL injection)
	$myusername = stripslashes($myusername);
	$mypassword = stripslashes($mypassword);
	$myusername = mysql_real_escape_string($myusername);
	$mypassword = mysql_real_escape_string($mypassword);

	// Since we have fetched user data, lets now check if user is approved (0 means NOT approved)
	$sql="SELECT * FROM $tbl_name WHERE phonenumber = '$myusername' AND password = '$mypassword' AND count_approval = 1 " ;
	$result=mysql_query($sql);

	// Mysql_num_row is counting table row
	$count=mysql_num_rows($result);

	// If result matched $myusername and $mypassword, table row must be 1 row
	if($count==1){

		// Register $myusername, $mypassword and redirect to file "DASHBORDS"
		$row = mysql_fetch_row($result);

			$_SESSION['id_user']=$row[0];
			$_SESSION['phonenumber']=$row[1];
			$_SESSION['usertype']=$row[2];
			$_SESSION['page']='dashboard.php';

				if($row[2]=="admin"){
					$msg = "Welcome";
					header("location:../pages/admin_files/admins_dashboard.php?msg=$msg");
				break;
				}elseif($row[2]=="guest"){

						$msg = "Welcome";
					 	header("location:../pages/guest_files/guest_dashboard.php?msg=$msg");
						break;
					{
						header("location:../signup.php");
						break;
					}
				}elseif($row[2]=="agent"){

						$msg = "Welcome";
					 	header("location:../pages/agent_files/agent_dashboard.php?msg=$msg");
						break;
					{
						header("location:../signup.php");
						break;
					}
				}elseif($row[2]=="tenant"){

						$msg = "Welcome";
					 	header("location:../pages/tenant_files/tenant_dashboard.php?msg=$msg");
						break;
					{
						header("location:../signup.php");
						break;
					}
				}

		else 
		$msg = "Error login! No valid USER-TYPE! check with the system administrator for assistance.";
			header("location:../../index.php?msg=$msg");
	}else {
		
	$msg = "Error login! Check that you have entered your phone number and password correctly or check with the system administrator to verify your account.";
		header("location:../../index.php?msg=$msg");
	}
