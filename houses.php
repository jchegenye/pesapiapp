<!DOCTYPE html>
<html lang="en">
  <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    	<title>Houses | Online Estate Managment System</title>

    	<?php include("views/partials/css-links.php"); ?>

	</head>

	<body>
	    <nav class="navbar navbar-default navbar-fixed-top">
	        <div class="container">
	            <!-- Brand and toggle get grouped for better mobile display -->
	            <div class="navbar-header page-scroll">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="#page-top">O E M S</a>
	            </div>

	            <!-- Collect the nav links, forms, and other content for toggling -->
	            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	                <ul class="nav navbar-nav navbar-right">
	                    <li class="hidden">
	                        <a href="#page-top"></a>
	                    </li>
	                    <li class="page-scroll">
	                        <a href="home.php">Home</a>
	                    </li>
	                    <li class="page-scroll">
	                        <a href="apartments.php">Apartments</a>
	                    </li>
	                    <li class="page-scroll">
	                        <a href="index.php">Login</a>
	                    </li>
	                </ul>
	            </div>
	            <!-- /.navbar-collapse -->
	        </div>
	        <!-- /.container-fluid -->
	    </nav>

		<div class="container">
			<div style=" margin-top: 65px;">
				<p>
    		        <?php

						if(isset($_GET['msg'])){
						
							$data=$_GET['msg'];
							$msg =''.$data.'';

								?>
									<div class="alert alert-danger text-center">
										<?php echo $msg; ?>	
									</div>
								<?php
						}

					?> 
			    </p>

				<?php 

					include "views/dbconnector.php";

					$i=1; $no=$page-1; $no=$no*$limit;

                   $SQL = "SELECT * FROM houses WHERE house_state = 'Vaccant'";
						$result = mysql_query($SQL);	
						while ($db_field = mysql_fetch_assoc($result)) {

                ?>
				<div class="col-md-4">
					<div >
						<img style="width: 100%; height:300px;" src="public/assets/uploaded/<?php echo $db_field['house_photo'];  ?>" class="girl img-responsive" alt="" />
						<div style="
						    padding-left: 15px;
						    padding-top: 10px;
						    padding-bottom: 10px;
						    background: #FFFAF2;
						">
							<h4><b><?php echo $db_field['house_name']; ?></b></h4>
							<h5><?php echo $db_field['house_town']; ?> (<?php echo $db_field['house_estate']; ?>)</h5>
							<div class="panel panel-default" style="margin-left: -15px; margin-bottom: -5px;">
	                              <div class="panel-heading">
	                                  <h4 class="panel-title">
	                                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $no+$i; ?>">
	                                          Find Out More
	                                      </a>
	                                  </h4>
	                              </div>
	                              <div id="<?php echo $no+$i; ?>" class="panel-collapse collapse">
	                                  <div class="panel-body">
	                                      	Price: <?php echo $db_field['house_rent']; ?>
	                                      	<br>
	                                      	<?php echo $db_field['house_description']; ?>
	                                   
	                                      	<form onsubmit="return checkForm(this);" class="login-form" action="inquire.php" method="POST">
		                                      	<input type="hidden" name="inquire_house_id" value="<?php echo $db_field['id_houses']; ?>">
		                                      	<input type="hidden" name="inquire_date" value="<?php echo date('Y-m-d'); ?>">
									            <h4 style="    background: #FFF;
												    margin-top: -30px;
												    padding-bottom: 10px;
												    padding-top: 10px;
												    color: #4682B4;
												    border-top: 1px solid #D5D7DE;">Inquire about this house</h4>

									            <div class="input-group">
									              <span class="input-group-addon"><i class="icon_profile"></i></span>
									              <input type="text" name="inquire_name" class="form-control" placeholder="Full Names" autofocus>
									            </div>
									            <div class="input-group">
									              <span class="input-group-addon"><i class="icon_phone"></i></span>
									              <input type="text" name="inquire_phonenumber" class="form-control" placeholder="Phone Number" autofocus>
									            </div>
									            <div class="input-group">
									              <span class="input-group-addon"><i class="icon_profile"></i></span>
									              <textarea name="inquire_message" class="form-control" placeholder="Write something"></textarea>
									            </div>
									             <button class="btn btn-lg btn-block" type="submit">Submit</button>
								            </form>
	                                  </div>
	                              </div>
	                          </div>

						</div>
					</div>
				</div>
				<?php $i++;

                    }

                ?>
			</div>
		</div>

		<div class="container">
			<h5 class="text-center">Developed by jchegenye</h5>
		</div>
		<?php include("views/partials/js-links.php"); ?>
	</body>

</html>