-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 29, 2016 at 03:11 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `patrick`
--

-- --------------------------------------------------------

--
-- Table structure for table `apartments`
--

CREATE TABLE IF NOT EXISTS `apartments` (
  `apart_id` int(11) NOT NULL AUTO_INCREMENT,
  `apartment_names` varchar(500) NOT NULL,
  `house_units` varchar(500) NOT NULL,
  `parking` varchar(500) NOT NULL,
  `town` varchar(500) NOT NULL,
  `estate` varchar(500) NOT NULL,
  `apartment_photo` text NOT NULL,
  `registered_date` text NOT NULL,
  PRIMARY KEY (`apart_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `apartments`
--

INSERT INTO `apartments` (`apart_id`, `apartment_names`, `house_units`, `parking`, `town`, `estate`, `apartment_photo`, `registered_date`) VALUES
(23, 'Afraha Apartments', '20', 'Yes', 'Nairobi', 'Mwiki', 'pic6.png', '2009-07-01'),
(26, 'WEMA APARTMENT', '40', 'Yes', 'KASARANI', 'HUNTERS', 'banner4.jpg', '2016-06-17');

-- --------------------------------------------------------

--
-- Table structure for table `houses`
--

CREATE TABLE IF NOT EXISTS `houses` (
  `id_houses` int(11) NOT NULL AUTO_INCREMENT,
  `house_id` text NOT NULL,
  `apartment_id` text NOT NULL,
  `rent` text NOT NULL,
  `deposit` text NOT NULL,
  `state` text NOT NULL,
  `tenant_id` text NOT NULL,
  `house_photo` text NOT NULL,
  `description` text NOT NULL,
  `registered_date` text NOT NULL,
  PRIMARY KEY (`id_houses`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `houses`
--

INSERT INTO `houses` (`id_houses`, `house_id`, `apartment_id`, `rent`, `deposit`, `state`, `tenant_id`, `house_photo`, `description`, `registered_date`) VALUES
(6, 'A6', '25', '12000', '6000', 'Occupied', '28247294', 'hse.png', 'Two Bed', '2009-07-01'),
(7, 'A7', '23', '24000', '5000', 'Vacant', '28247294', 'hse.png', '2 bed', '2016-06-14');

-- --------------------------------------------------------

--
-- Table structure for table `pesapi_account`
--

CREATE TABLE IF NOT EXISTS `pesapi_account` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `push_in` tinyint(1) NOT NULL,
  `push_out` tinyint(1) NOT NULL,
  `push_neutral` tinyint(1) NOT NULL,
  `settings` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_index` (`type`),
  KEY `definedby` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesapi_account`
--

INSERT INTO `pesapi_account` (`id`, `type`, `name`, `identifier`, `push_in`, `push_out`, `push_neutral`, `settings`) VALUES
(0, 2, '', '0711494289', 0, 0, 0, 'a:7:{s:11:"PUSH_IN_URL";s:0:"";s:14:"PUSH_IN_SECRET";s:0:"";s:12:"PUSH_OUT_URL";s:0:"";s:15:"PUSH_OUT_SECRET";s:0:"";s:16:"PUSH_NEUTRAL_URL";s:0:"";s:19:"PUSH_NEUTRAL_SECRET";s:0:"";s:11:"SYNC_SECRET";s:8:"zn3wbhrq";}'),
(1, 2, '', '', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `pesapi_payment`
--

CREATE TABLE IF NOT EXISTS `pesapi_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `super_type` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `receipt` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `phonenumber` varchar(45) NOT NULL,
  `name` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `post_balance` bigint(20) NOT NULL,
  `note` varchar(255) NOT NULL,
  `transaction_cost` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_index` (`type`),
  KEY `name_index` (`name`),
  KEY `phone_index` (`phonenumber`),
  KEY `time_index` (`time`),
  KEY `super_index` (`super_type`),
  KEY `fk_mpesapi_payment_account_idx` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pesapi_payment`
--

INSERT INTO `pesapi_payment` (`id`, `account_id`, `super_type`, `type`, `receipt`, `time`, `phonenumber`, `name`, `account`, `status`, `amount`, `post_balance`, `note`, `transaction_cost`) VALUES
(1, 1, 0, 0, 'MPESA XYYSFDSFFSDF', '2016-06-06 03:11:10', '0711494289', 'Jackson Chegenye', 'Mpesa', 0, 8000, 27, 'payment complete', 35);

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE IF NOT EXISTS `tenants` (
  `id_tenant` int(11) NOT NULL AUTO_INCREMENT,
  `idno` text NOT NULL,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `tenant_phone` text NOT NULL,
  `tenant_photo` text NOT NULL,
  `registered_date` text NOT NULL,
  `house_id` text NOT NULL,
  PRIMARY KEY (`id_tenant`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id_tenant`, `idno`, `fname`, `lname`, `email`, `address`, `tenant_phone`, `tenant_photo`, `registered_date`, `house_id`) VALUES
(2, '28247294', 'Jackson', 'Chegenye', 'chegenyejackson@gmail.com', 'Kimathi', '+254711494289', 'IMG-20150712-WA0003.jpg', '2016-06-14', ''),
(6, '', '', '', '', '', '', '', '2016-06-14', ''),
(7, '345345345', 'Pato', 'Part', 'pato@gmail.com', 'Kimathi', '711494289', 'apart.png', '2016-06-17', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(500) NOT NULL,
  `usertype` varchar(500) NOT NULL,
  `names` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `registered_date` text NOT NULL,
  `count_approval` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `phonenumber`, `usertype`, `names`, `password`, `registered_date`, `count_approval`) VALUES
(11, '0711494289', 'admin', 'Jackson Chegenye', '0192023a7bbd73250516f069df18b500', '2009-07-01', 1),
(12, '0722494289', 'agent', 'Stellah Asumu', '2ec199f1e2de31576869a57488e919ad', '2009-07-01', 1),
(14, '0725035390', 'guest', 'patrick', '1c8f6e53805492003abe7003692831b2', '2016-06-17', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pesapi_payment`
--
ALTER TABLE `pesapi_payment`
  ADD CONSTRAINT `fk_mpesapi_payment_account` FOREIGN KEY (`account_id`) REFERENCES `pesapi_account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
